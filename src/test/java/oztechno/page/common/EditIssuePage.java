package oztechno.page.common;

import org.junit.internal.runners.statements.Fail;
import org.junit.runner.notification.Failure;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import oztechno.page.common.PageParent;

public class EditIssuePage extends PageParent {

	/**
	 * Set value in Summary Field
	 * 
	 */
	public void setValueInSummaryField(String summary) {
		
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement summaryField = null;
		try {
			summaryField = webDriver.findElement(By.id("summary"));
			summaryField.clear();
			summaryField.sendKeys(summary);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Summary field.");
		}
		
		if (debugMode) {
        	System.out.println("Leaving");
        }
		
	}
	
	/**
	 * Set value in description Field
	 * 
	 */
	public void setValueInDescriptionField(String description) {
		
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement descriptionField = null;
		try {
			descriptionField = webDriver.findElement(By.id("description"));
			descriptionField.clear();
			descriptionField.sendKeys(description);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the description field.");
		}
		
		if (debugMode) {
        	System.out.println("Leaving");
        }
		
	}
	
	/**
	 * Select a specific user - leave as Automatic for testing
	 * TODO id = assignee-field
	 */
	
	/**
	 * Select Issue Type
	 * TODO id = issuetype-field
	 */

	/**
	 * Clicks the Update button
	 * 
	 */
	public void clickUpdateButton() {
		
		if (debugMode) {
	    	System.out.println("Entered");
	    }

        // Verify element exists and interact with it
		WebElement updateButton = null;
		try {
			updateButton = webDriver.findElement(By.id("edit-issue-submit"));
			updateButton.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Update button.");
		}
		if (debugMode) {
	    	System.out.println("Leaving");
	    }

	}

}
