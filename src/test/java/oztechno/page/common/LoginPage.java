package oztechno.page.common;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import oztechno.page.common.PageParent;

public class LoginPage extends PageParent {
	
	/**
	 * Opens the login Page.
	 *
	 */
	public void openLoginPage() {

        if (debugMode) {
        	System.out.println("Entered");
        }

        String baseUrl = testParams.getProperty("base_url");
        webDriver.get(baseUrl);

        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
	/**
	 * Click the Login link
	 * 
	 */
	public void clickLoginLink() {
		
        if (debugMode) {
        	System.out.println("Entered");
        }
        
        // Verify element exists and interact with it
        WebElement loginLink = null;
        try {
			loginLink = webDriver.findElement(By.linkText("Log In"));
			loginLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Login Link.");
		}

        if (debugMode) {
        	System.out.println("Leaving");
        }
		
	}
	
	/**
	 * Set the value in Username field
	 * 
	 */
	public void setValueInUsernameField(String username) {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement usernameField = null;
		try {
			usernameField = webDriver.findElement(By.id("username"));
			usernameField.sendKeys(username);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Username field.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
	/**
	 * Set the value in Password field
	 * 
	 */
	public void setValueInPasswordField(String password) {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement passwordField = null;
		try {
			passwordField = webDriver.findElement(By.id("password"));
			passwordField.sendKeys(password);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Password field.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}

	/**
	 * Click the Login button
	 * 
	 */
	public void clickLoginButton() {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement loginBtn = null;
		try {
			loginBtn = webDriver.findElement(By.id("login-submit"));
			loginBtn.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Login button.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}

}
