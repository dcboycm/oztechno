package oztechno.page.common;

import org.junit.internal.runners.statements.Fail;
import org.junit.runner.notification.Failure;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import oztechno.page.common.PageParent;

public class CreateIssuePage extends PageParent {
	
	// Only creating methods for required fields or commonly used elements

	//TODO Create method to automatically create ticket
	// url = base + /secure/CreateIssue!default.jspa
	
	/**
	 * Select Project Type
	 * TODO id = project-field
	 */
	
	/**
	 * Select Issue Type
	 * TODO id = issuetype-field
	 */
	
	/**
	 * Set value in Summary Field
	 * 
	 */
	public void setValueInSummaryField(String summary) {
		
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement summaryField = null;
		try {
			summaryField = webDriver.findElement(By.id("summary"));
			summaryField.sendKeys(summary);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Summary field.");
		}
		
		if (debugMode) {
        	System.out.println("Leaving");
        }
		
	}
	
	/**
	 * Select a specific user - leave as Automatic for testing
	 * TODO id = assignee-field
	 */
	
	/**
	 * Set value in description Field
	 * 
	 */
	public void setValueInDescriptionField(String description) {
		
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement descriptionField = null;
		try {
			descriptionField = webDriver.findElement(By.id("description"));
			descriptionField.sendKeys(description);
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the description field.");
		}
		
		if (debugMode) {
        	System.out.println("Leaving");
        }
		
	}
	
	/**
	 * Click the Create Another Checkbox
	 * 
	 */
	public void clickCreateAnotherCheckbox() {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement createAnotherCheckbox = null;
		try {
			createAnotherCheckbox = webDriver.findElement(By.id("qf-create-another"));
			createAnotherCheckbox.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Create Another Checkbox.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}

	
	/**
	 * Click the Create button
	 * 
	 */
	public void clickCreateButton() {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement createButton = null;
		try {
			createButton = webDriver.findElement(By.id("create-issue-submit"));
			createButton.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Create button.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}
	
	/**
	 * Click the Cancel Link
	 * 
	 */
	public void clickCancelLink() {
        
		if (debugMode) {
        	System.out.println("Entered");
        }
		
        // Verify element exists and interact with it
		WebElement cancelLink = null;
		try {
			cancelLink = webDriver.findElement(By.linkText("Cancel"));
			cancelLink.click();
		} catch (NoSuchElementException e) {
			System.out.println("Could not find the Cancel link.");
		}
		
        if (debugMode) {
        	System.out.println("Leaving");
        }

	}

}
