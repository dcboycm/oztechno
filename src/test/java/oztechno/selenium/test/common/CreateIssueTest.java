package oztechno.selenium.test.common;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import oztechno.page.common.BrowsePage;
import oztechno.page.common.CreateIssuePage;
import oztechno.selenium.BaseSeleniumTest;

public class CreateIssueTest extends BaseSeleniumTest{
	
	private BrowsePage browsePage = new BrowsePage();
	private CreateIssuePage createIssuePage = new CreateIssuePage();
	
	@Test
	public void RunThisFirst() {
		
		String summary = testData.getString("TICKET_SUMMARY");
		String description = testData.getString("TICKET_DESCRIPTION");
		String searchableItem = testData.getString("SEARCHABLE_STRING");
		
		browsePage.openBrowsePage();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("create_link")));
		browsePage.clickCreateButton();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary")));
		createIssuePage.setValueInSummaryField(summary);
		createIssuePage.setValueInDescriptionField(description);
		createIssuePage.clickCreateButton();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("success")));
		
		// Verify the ticket was created
		browsePage.setValueInSearchField(searchableItem);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Reported by Me")));
		browsePage.clickReportedByMeLink();
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.className("search-title"), "Reported by Me"));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("summary-val")));
		Assert.assertEquals(summary, webDriver.findElement(By.id("summary-val")).getText());
		
	}

}
